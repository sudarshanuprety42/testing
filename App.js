import React, { useState } from "react";
import * as Font from "expo-font";
import Home from "./screens/Home";
import { AppLoading } from "expo";
import {
  createStackNavigator,
  CardStyleInterpolators
} from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import ReviewDetails from "./screens/ReviewDetails";
import MoreReviewDetails from "./screens/MoreReviewDetails";
import LogoTitle from "./screens/LogoTitle";
import { Button } from "react-native";

const getFonts = () => {
  return Font.loadAsync({
    "nunito-regular": require("./assets/fonts/Nunito-Regular.ttf"),
    "nunito-bold": require("./assets/fonts/Nunito-Bold.ttf")
  });
};

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

export default function App() {
  const [fontsLoaded, setFontsLoaded] = useState(false);

  if (fontsLoaded) {
    return (
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            headerStyle: {
              backgroundColor: "#333"
            },
            headerTintColor: "#fff",
            headerTitleStyle: {
              fontWeight: "bold"
            }
          }}
        >
          <Stack.Screen
            name="Home"
            component={Home}
            options={{
              headerTitle: () => <LogoTitle />,
              headerRight: () => (
                <Button
                  onPress={() => alert("This is a button!")}
                  title="Info"
                  color="black"
                />
              ),
              headerStyle: {
                backgroundColor: "#f4511e"
              },
              headerTintColor: "#fff",
              headerTitleStyle: {
                fontWeight: "bold"
              }
            }}
          />
          <Stack.Screen name="ReviewDetails" component={ReviewDetails} />
          <Stack.Screen
            name="MoreReviewDetails"
            component={MoreReviewDetails}
            initialParams={{ details: "Details recieved as initial params" }}
          />
        </Stack.Navigator>
        {/* <Drawer.Navigator
          initialRouteName="Home"
          drawerType="front"
          drawerStyle={{
            backgroundColor: "#c6cbef",
            width: 240
          }}
          
        >
          <Drawer.Screen name="Home" component={Home} options={{
            drawerLabel:"Home"
          }}/>
          <Drawer.Screen name="ReviewDetails" component={ReviewDetails} />
        </Drawer.Navigator>*/}
      </NavigationContainer> 
          );
  } else {
    return (
      <AppLoading startAsync={getFonts} onFinish={() => setFontsLoaded(true)} />
    );
  }
}
