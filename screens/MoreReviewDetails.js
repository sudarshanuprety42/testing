import React from "react";
import { View, Text } from "react-native";

export default function MoreReviewDetails({ route }) {
  const { details, detailsFromParent } = route.params;
  return (
    <View>
      <Text>More Review Details</Text>
      <Text>{details}</Text>
      <Text>{detailsFromParent}</Text>
    </View>
  );
}
