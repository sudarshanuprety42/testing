import React from "react";
import { View, Text, Button } from "react-native";
import { globalStyles } from "../styles/global";
import MyHeader from "./Header"

export default function ReviewDetails({route, navigation }) {
  const { itemId, otherParam } = route.params;

  const [count, setCount] = React.useState(0);

  navigation.setOptions({
    headerRight: () => (
      <Button onPress={() => setCount(c => c + 1)} title="Update count" />
    ),
  });
  
  return (
    <View style={globalStyles.container}>
      {/* <MyHeader navigation={navigation}/> */}
      <Text>Review Details Screen</Text>
      <Text>Details Screen</Text>
      <Text>itemId: {JSON.stringify(itemId)}</Text>
      <Text>otherParam: {JSON.stringify(otherParam)}</Text>
      <Button
        title="Go to Details... again"
        onPress={() =>
          navigation.push('ReviewDetails', {
            itemId: Math.floor(Math.random() * 100),
          })
        }
      />
      <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
      <Button title="Go back" onPress={() => navigation.goBack()} />
      <Button
        title="Go to More Review Details"
        onPress={() => navigation.navigate("MoreReviewDetails", 
        {
          detailsFromParent: "Hey this is details passed from parent"
        })}
      />
      <Button
        title="Update the title"
        onPress={() => navigation.setOptions({ title: "Updated!" })}
      />
      <Text>{count}</Text>
    </View>
  );
}
