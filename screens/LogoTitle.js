import React from 'react'
import { Text } from 'react-native'
import { globalStyles } from '../styles/global'

export default function LogoTitle() {
  return (
    <Text style={globalStyles.titleText}>
      My Home
    </Text>
  )
}
