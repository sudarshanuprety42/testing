import React from "react";
import { View, Text, Button } from "react-native";
import { globalStyles } from "../styles/global";
import { Header, Icon, Left } from "native-base";
import Constants from "expo-constants";
import { TouchableHighlight, TouchableNativeFeedback } from "react-native-gesture-handler";
import MyHeader from "./Header"

export default function Home({ navigation }) {
  return (
    <View style={globalStyles.container}>
      {/* <MyHeader navigation={navigation}/> */}
      <Text>Home Screen</Text>
      <Button
        title="Go to Details"
        onPress={() =>
          navigation.navigate("ReviewDetails", {
            itemId: 86,
            otherParam: "anything you want here"
          })
        }
      />
    </View>
  );
}
