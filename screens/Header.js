import React from "react";
import { TouchableNativeFeedback } from "react-native-gesture-handler";
import { Header, Icon, Left } from "native-base";
import Constants from "expo-constants";
import { View, Text, Button } from "react-native";

export default function MyHeader(props) {
  return (
    <Header style={{ height: 80 }}>
      <Left
        style={{
          flex: 1,
          marginTop: Constants.statusBarHeight,
          flexDirection: "row",
          justifyContent: "flex-start"
        }}
      >
        <TouchableNativeFeedback onPress={() => props.navigation.toggleDrawer()}>
          <Icon name="menu" />
        </TouchableNativeFeedback>
        <Text style={{ fontSize: 20, marginLeft: 20 }}>Home</Text>
      </Left>
    </Header>
  );
}
